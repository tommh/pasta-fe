import React from "react";
import { StyleSheet } from "react-native";
import { ShoppingListScreen } from "pasta/screen/shoppingList/ShoppingListScreen";
import { MealsScreen } from "./src/screen/meal/MealsScreen";
import { View, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import {
  getFocusedRouteNameFromRoute,
  useNavigationState,
} from "@react-navigation/native";

const Drawer = createDrawerNavigator();

export function determineLeftNavButton(route) {
  const state = useNavigationState((state) => state);
  if (state.index > 0) {
    // How to check here: get rout children
    console.log("Create a back button");
  } else {
    console.log("Create Hamburger menu");
  }
  return (
    <Button
      onPress={() => /*props.navigation.openDrawer()*/ console.log("CLICK")}
      title="Info"
      color="#fff"
    />
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Shopping List">
        <Drawer.Screen
          name="Shopping List"
          component={ShoppingListScreen}
          options={({ route }) => ({
            headerRight: () => determineLeftNavButton(route),
          })}
        />
        <Drawer.Screen
          name="Meals"
          component={MealsScreen}
          options={({ route }) => ({
            headerRight: () => determineLeftNavButton(route),
          })}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
