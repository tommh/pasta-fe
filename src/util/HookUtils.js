import { useEffect } from "react";

export const onLoadAsync = (setter, getter, args) =>
  useEffect(
    () =>
      (async function () {
        setter(await getter(args));
      })(),
    []
  );
