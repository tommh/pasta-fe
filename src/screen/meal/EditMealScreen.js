import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { useState } from "react";
import { onLoadAsync } from "pasta/util/HookUtils";
import { getMeal } from "pasta/client/MealClient";

export const EditMealScreen = (props) => {
  const [meal, setMeal] = useState(null);

  onLoadAsync(setMeal, getMeal, props.id);

  if (meal == null) return <Text>Loading meal..</Text>;

  return <Meal meal={meal} />;
};

const Meal = (props) => {
  return (
    <View>
      <Text>Edit meal: </Text>
      <Text>{props.meal.name} </Text>
    </View>
  );
};
