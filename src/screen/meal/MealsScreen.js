import React from "react";
import { StyleSheet, Text, View, Button, Icon } from "react-native";
import { useState } from "react";
import { onLoadAsync } from "pasta/util/HookUtils";
import { getMeals } from "pasta/client/MealClient";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { EditMealScreen } from "./EditMealScreen";

const Stack = createNativeStackNavigator();

export const MealsScreen = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="All Meals"
        component={MealsMainScreen}
        navigation={props.navigation}
      />
      <Stack.Screen name="Meal" component={EditMealScreen} />
    </Stack.Navigator>
  );
};

const MealsMainScreen = (props) => {
  const [meals, setMeals] = useState([]);

  onLoadAsync(setMeals, getMeals);
  return (
    <View>
      <Button
        onPress={() => props.navigation.navigate("DrawerOpen")}
        title="Open drawer"
      />
      {meals.map((meal, key) => {
        return <Meal meal={meal} navigation={props.navigation} key={key} />;
      })}
    </View>
  );
};

const Meal = (props) => {
  return (
    <View>
      <Text>{props.meal.name} </Text>
      <Button
        title="view"
        onPress={() => props.navigation.navigate("Meal", { id: props.meal.id })}
      />
    </View>
  );
};
