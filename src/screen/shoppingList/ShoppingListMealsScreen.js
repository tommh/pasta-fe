import React from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { getShoppingListMeals } from "pasta/client/ShoppingListClient";
import { onLoadAsync } from "pasta/util/HookUtils";

export const ShoppingListMealsScreen = (props) => {
  const [shoppingListMeals, setShoppingListMeals] = useState([]);

  onLoadAsync(
    setShoppingListMeals,
    getShoppingListMeals,
    props.shoppingList.id
  );

  return (
    <View>
      {shoppingListMeals.map((meal, key) => {
        return <ShoppingListMeal meal={meal} key={key} />;
      })}
    </View>
  );
};

const ShoppingListMeal = (props) => {
  const meal = props.meal;
  if (meal == null) return <Text>No Meal</Text>;

  return (
    <View>
      <Text>{meal.name}</Text>
      <Text></Text>
    </View>
  );
};
