import React from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { getShoppingListItems } from "pasta/client/ShoppingListClient";
import { onLoadAsync } from "pasta/util/HookUtils";

export const ShoppingListItemsScreen = (props) => {
  const [shoppingListItems, setShoppingListItems] = useState([]);

  onLoadAsync(
    setShoppingListItems,
    getShoppingListItems,
    props.shoppingList.id
  );

  return (
    <View>
      {shoppingListItems.map((item, key) => {
        return <ShoppingListItem item={item} key={key} />;
      })}
    </View>
  );
};

const ShoppingListItem = (props) => {
  const item = props.item;
  if (item == null) return <Text>No Item</Text>;

  return (
    <View>
      <Text>{item.product?.name}</Text>
      <Text>{item.quantity?.amount}</Text>
      <Text>{item.quantity?.quantityType}</Text>
      <Text>{showCheck(item.checked)}</Text>
      <Text></Text>
    </View>
  );
};

const showCheck = (checked) => {
  if (checked) {
    return "V";
  } else return "X";
};
