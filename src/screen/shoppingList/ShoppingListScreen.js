import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useState } from "react";
import { getShoppingList } from "pasta/client/ShoppingListClient";
import { onLoadAsync } from "pasta/util/HookUtils";
import { ShoppingListItemsScreen } from "./ShoppingListItemsScreen";
import { ShoppingListMealsScreen } from "./ShoppingListMealsScreen";
import { ShoppingListProductsScreen } from "./ShoppingListIProductsScreen";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

export const ShoppingListScreen = (props) => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Pasta" component={ShoppingListMainScreen} />
    </Stack.Navigator>
  );
};

const ShoppingListMainScreen = (props) => {
  const [shoppingList, setShoppingList] = useState(null);

  onLoadAsync(setShoppingList, getShoppingList);

  if (shoppingList != null) {
    return shoppingListContainer(shoppingList, props.navigation);
  } else {
    return NotFoundScreen;
  }
};

const Tab = createBottomTabNavigator();

const shoppingListContainer = (shoppingList, navigation) => (
  <Tab.Navigator>
    <Tab.Screen
      name="All Items"
      children={() => (
        <ShoppingListItemsScreen
          shoppingList={shoppingList}
          navigation={navigation}
        />
      )}
    />
    <Tab.Screen
      name="Meals"
      children={() => <ShoppingListMealsScreen shoppingList={shoppingList} />}
    />
    <Tab.Screen
      name="Products"
      children={() => (
        <ShoppingListProductsScreen shoppingList={shoppingList} />
      )}
    />
  </Tab.Navigator>
);

// function SettingsScreen(props) {
//   return (
//     <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
//       <Text>Settings! {props.shoppingList?.name}</Text>
//     </View>
//   );
// }

const ProductsScreen = (props) => {
  return (
    <View>
      <Text>Products Screen {props.shoppingList?.name}</Text>
    </View>
  );
};

const NotFoundScreen = (
  <View>
    <Text>Cannot Retrieve shopping list</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
  },
});
