import React from "react";
import { useState } from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { getShoppingListProducts } from "pasta/client/ShoppingListClient";
import { onLoadAsync } from "pasta/util/HookUtils";

export const ShoppingListProductsScreen = (props) => {
  const [shoppingListProducts, setShoppingListProducts] = useState([]);

  onLoadAsync(
    setShoppingListProducts,
    getShoppingListProducts,
    props.shoppingList.id
  );

  return (
    <View>
      {shoppingListProducts.map((product, key) => {
        return <ShoppingListProduct product={product} key={key} />;
      })}
    </View>
  );
};

const ShoppingListProduct = (props) => {
  const product = props.product;
  if (product == null) return <Text>No Procuct</Text>;

  return (
    <View>
      <Text>{product.product?.name}</Text>
      <Text>{product.quantity?.amount}</Text>
      <Text>{product.quantity?.quantityType}</Text>
      <Text></Text>
    </View>
  );
};

const showCheck = (checked) => {
  if (checked) {
    return "V";
  } else return "X";
};
