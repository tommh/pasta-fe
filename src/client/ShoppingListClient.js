import React from "react";
import { Config } from "../Constants";
import { get } from "./BaseClient";

export const getShoppingList = () => get("/v1/shoppingLists/1");

export const getShoppingListItems = (id) =>
  get("/v1/shoppingLists/" + id + "/items");

export const getShoppingListMeals = (id) =>
  get("/v1/shoppingLists/" + id + "/meals");

export const getShoppingListProducts = (id) =>
  get("/v1/shoppingLists/" + id + "/entries");
