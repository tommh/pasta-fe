import { Config } from "../Constants";

export const get = async (path) => {
  return fetch(Config.baseUrl + path)
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.error(error);
      return null;
    });
};
