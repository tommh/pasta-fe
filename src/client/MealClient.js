import React from "react";
import { Config } from "../Constants";
import { get } from "./BaseClient";

export const getMeals = () => get("/v1/meals");

export const getMeal = (id) => get("/v1/meals/" + id);
