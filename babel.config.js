module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo", "module:metro-react-native-babel-preset"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["."],
          extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
          alias: {
            pasta: "./src/",
            screen: "./src/screen",
            client: "./src/client",
          },
        },
      ],
    ],
  };
};

// module.exports = {
//   presets: ["module:metro-react-native-babel-preset"],
//   plugins: [
//     [
//       "module-resolver",
//       {
//         root: ["."],
//         extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
//         alias: {
//           pasta: "./src/",
//           constants: "./src/screen",
//           client: "./src/client",
//         },
//       },
//     ],
//   ],
// };
