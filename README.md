## Debugging

`npm start`

Run emulator
`emulator -avd Pixel_3_API_30`
Or if your emulator name is different (emulator -list-avds)

Set emulator in debug mode
`CMD + M > Remote Debugging`

Close chrome debugger window (check that port is 19000, else use the actual port)
In Visual Studio make sure that react-native.packager.port is set to 19000

Debug in VSCode

## Beginners guide

https://www.youtube.com/watch?v=0-S5a0eXPoc&t=3452s
